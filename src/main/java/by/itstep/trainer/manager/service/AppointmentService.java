package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;

import java.util.List;

public interface AppointmentService {

    List<AppointmentPreviewDto> findAll();

    AppointmentFullDto findById(Long id);

    AppointmentFullDto create(AppointmentCreateDto createDto);

    AppointmentFullDto update(AppointmentUpdateDto updateDto);

    void delete(Long id);

    void deleteAllForTrainer(Long trainerId);
}
