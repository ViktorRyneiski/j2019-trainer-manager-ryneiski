package by.itstep.trainer.manager.service.impl;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.AppointmentMapper;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.service.AppointmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final AppointmentMapper appointmentMapper;
    private final TrainerRepository trainerRepository;

    @Override
    public List<AppointmentPreviewDto> findAll() {
        List<Appointment> found = appointmentRepository.findAll();

        List<AppointmentPreviewDto> dtos = appointmentMapper.mapToDtoList(found);
        return dtos;
    }

    @Override
    public AppointmentFullDto findById(final Long id) {

        Appointment found = appointmentRepository.findById(id);

        AppointmentFullDto dto = appointmentMapper.mapToDto(found);
        return dto;
    }

    @Override
    public AppointmentFullDto create(final AppointmentCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Appointment toSave = appointmentMapper.mapToEntity(createDto, trainer);
        toSave.setCreatedAt(new Timestamp((new java.util.Date()).getTime()));
        Appointment created = appointmentRepository.create(toSave);

        AppointmentFullDto dto = appointmentMapper.mapToDto(created);

        return dto;
    }

    @Override
    public AppointmentFullDto update(final AppointmentUpdateDto updateDto) {
        Appointment existingEntity = appointmentRepository.findById(updateDto.getId());
        Appointment toUpdate = appointmentMapper.mapToEntity(updateDto);
        toUpdate.setTrainer(existingEntity.getTrainer());
        toUpdate.setCreatedAt(existingEntity.getCreatedAt());

        Appointment updated = appointmentRepository.update(toUpdate);
        AppointmentFullDto dto = appointmentMapper.mapToDto(updated);

        return dto;
    }

    @Override
    public void delete(final Long id) {
        appointmentRepository.delete(id);
    }

    @Override
    public void deleteAllForTrainer(final Long trainerId) {
        List<Appointment> appointments = trainerRepository.findById(trainerId).getAppointments();
        for (Appointment a : appointments) {
            appointmentRepository.delete(a.getId());
        }
    }
}
