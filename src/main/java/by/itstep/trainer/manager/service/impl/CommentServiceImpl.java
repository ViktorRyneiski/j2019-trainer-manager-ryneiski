package by.itstep.trainer.manager.service.impl;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.Comment;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.CommentMapper;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.service.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final TrainerRepository trainerRepository;

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> found = commentRepository.findAll();
        System.out.println("CommentServiceImpl -> found " + found.size() + " comments");

        return commentMapper.mapToDtoList(found);
    }

    @Override
    public CommentFullDto findById(final Long id) {
        Comment found = commentRepository.findById(id);
        System.out.println("CommentServiceImpl -> found comment " + found);

        return commentMapper.mapToDto(found);
    }

    @Override
    public CommentFullDto create(final CommentCreateDto createDto) {
        Trainer trainer = trainerRepository.findById(createDto.getTrainerId());
        Comment toCreate = commentMapper.mapToEntity(createDto, trainer);

        //check comment if ok setPublished
        toCreate.setPublished(true);

        Comment created = commentRepository.create(toCreate);
        System.out.println("CommentServiceImpl -> create comment " + created);

        return commentMapper.mapToDto(created);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {
        Comment toUpdate = commentMapper.mapToEntity(updateDto);
        Comment existingEntity = commentRepository.findById(updateDto.getId());
        toUpdate.setTrainer(existingEntity.getTrainer());
        toUpdate.setUserEmail(existingEntity.getUserEmail());
        toUpdate.setUserLastName(existingEntity.getUserLastName());
        toUpdate.setUserFirstName(existingEntity.getUserFirstName());
        toUpdate.setPublished(existingEntity.isPublished());

        Comment updated = commentRepository.update(toUpdate);
        System.out.println("CommentServiceImpl -> update comment " + updated);

        return commentMapper.mapToDto(updated);
    }

    @Override
    public void delete(final Long id) {
        commentRepository.delete(id);
        System.out.println("CommentServiceImpl -> comment with id: " + id + " was deleted");
    }

    @Override
    public void deleteAllForTrainer(final Long trainerId) {
        List<Comment> comments = trainerRepository.findById(trainerId).getComments();
        for (Comment c : comments) {
            commentRepository.delete(c.getId());
        }
    }
}
