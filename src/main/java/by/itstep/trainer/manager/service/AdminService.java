package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.dto.admin.AdminUpdateDto;

import java.util.List;

public interface AdminService {

    List<AdminPreviewDto> findAll();

    AdminFullDto findById(Long id);

    AdminFullDto findByEmail(String email);

    AdminFullDto create(AdminCreateDto createDto);

    AdminFullDto update(AdminUpdateDto updateDto);

    void delete(Long id);
}
