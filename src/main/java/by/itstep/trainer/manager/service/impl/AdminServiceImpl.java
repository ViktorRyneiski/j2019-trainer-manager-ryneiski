package by.itstep.trainer.manager.service.impl;

import by.itstep.trainer.manager.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.dto.admin.AdminUpdateDto;
import by.itstep.trainer.manager.entity.Admin;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.mapper.AdminMapper;
import by.itstep.trainer.manager.repository.AdminRepository;
import by.itstep.trainer.manager.service.AdminService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;

    private final AdminMapper adminMapper;

    @Override
    public List<AdminPreviewDto> findAll() {
        List<Admin> found = adminRepository.findAll();
        System.out.println("AdminServiceImpl -> found " + found.size() + " admins");

        List<AdminPreviewDto> dtos = adminMapper.mapToDtoList(found);
        return dtos;
    }

    @Override
    public AdminFullDto findById(final Long id) {
        Admin found = adminRepository.findById(id);
        System.out.println("AdminServiceImpl -> found admin " + found);

        AdminFullDto dto = adminMapper.mapToDto(found);
        return dto;
    }

    @Override
    public AdminFullDto findByEmail(final String email) {
        Admin found = adminRepository.findByEmail(email);
        System.out.println("AdminServiceImpl -> found admin " + found);

        AdminFullDto dto = adminMapper.mapToDto(found);
        return dto;
    }

    @Override
    public AdminFullDto create(final AdminCreateDto createDto) {
        Admin toSave = adminMapper.mapToEntity(createDto);

        toSave.setRole(Role.ADMIN);

        Admin created = adminRepository.create(toSave);
        System.out.println("AdminServiceImpl -> create admin " + created);

        AdminFullDto dto = adminMapper.mapToDto(created);
        return dto;
    }

    @Override
    public AdminFullDto update(final AdminUpdateDto updateDto) {
        Admin toUpdate = adminMapper.mapToEntity(updateDto);
        Admin existingEntity = adminRepository.findById(updateDto.getId());
        toUpdate.setEmail(existingEntity.getEmail());
        toUpdate.setPassword(existingEntity.getPassword());
        toUpdate.setRole(existingEntity.getRole());

        Admin updated = adminRepository.update(toUpdate);
        System.out.println("AdminServiceImpl -> update admin " + updated);

        AdminFullDto dto = adminMapper.mapToDto(updated);
        return dto;
    }

    @Override
    public void delete(final Long id) {
        adminRepository.delete(id);
        System.out.println("AdminServiceImpl -> admin with id: " + id + " was deleted");
    }
}
