package by.itstep.trainer.manager.service.impl;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.TrainerMapper;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.service.TrainerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TrainerServiceImpl implements TrainerService {

    private final TrainerRepository trainerRepository;

    private final TrainerMapper trainerMapper;

    @Override
    public List<TrainerPreviewDto> findAll() {
        List<Trainer> found = trainerRepository.findAll();
        System.out.println("TrainerServiceImpl -> found " + found.size() + " trainers");

        return trainerMapper.mapToDtoList(found);
    }

    @Override
    public TrainerFullDto findById(final Long id) {
        Trainer found = trainerRepository.findById(id);
        System.out.println("TrainerServiceImpl -> found trainer " + found);

        return trainerMapper.mapToDto(found);
    }

    @Override
    public TrainerFullDto create(final TrainerCreateDto createDto) {
        Trainer toCreate = trainerMapper.mapToEntity(createDto);
        Trainer created = trainerRepository.create(toCreate);
        System.out.println("TrainerServiceImpl -> create trainer " + created);

        return trainerMapper.mapToDto(created);
    }

    @Override
    public TrainerFullDto update(final TrainerUpdateDto updateDto) {
        Trainer toUpdate = trainerMapper.mapToEntity(updateDto);
        Trainer existingEntity = trainerRepository.findById(updateDto.getId());

        toUpdate.setPassword(existingEntity.getPassword());
        toUpdate.setComments(existingEntity.getComments());
        toUpdate.setAppointments(existingEntity.getAppointments());

        Trainer updated = trainerRepository.update(toUpdate);
        System.out.println("TrainerServiceImpl -> update trainer " + updated);

        return trainerMapper.mapToDto(updated);
    }

    @Override
    public void delete(final Long id) {
        trainerRepository.delete(id);
        System.out.println("TrainerServiceImpl -> trainer with id: " + id + " was deleted");
    }
}
