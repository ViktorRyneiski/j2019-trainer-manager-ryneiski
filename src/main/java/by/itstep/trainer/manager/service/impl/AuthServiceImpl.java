package by.itstep.trainer.manager.service.impl;

import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminLoginDto;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.service.AdminService;
import by.itstep.trainer.manager.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private AdminFullDto loginedAdmin;

    private final AdminService adminService;

    @Override
    public void login(final AdminLoginDto loginDto) {
        AdminFullDto foundAdmin = adminService.findByEmail(loginDto.getEmail());

        if (foundAdmin == null) {
            throw new RuntimeException("User not found by email: " + loginDto.getEmail());
        }
        if (!foundAdmin.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect!");
        }
        loginedAdmin = foundAdmin;
    }

    @Override
    public void logout() {
        loginedAdmin = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedAdmin != null;
    }

    @Override
    public Role getRole() {
        return loginedAdmin.getRole();
    }

    @Override
    public AdminFullDto getLoginedAdmin() {
        return loginedAdmin;
    }
}
