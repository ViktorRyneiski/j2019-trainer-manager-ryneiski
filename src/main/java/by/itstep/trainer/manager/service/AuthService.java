package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminLoginDto;
import by.itstep.trainer.manager.entity.enums.Role;

public interface AuthService {

    void login(AdminLoginDto loginDto);

    void logout();

    boolean isAuthenticated();

    Role getRole();

    AdminFullDto getLoginedAdmin();
}
