package by.itstep.trainer.manager.service;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;

import java.util.List;

public interface CommentService {

    List<CommentPreviewDto> findAll();

    CommentFullDto findById(Long id);

    CommentFullDto create(CommentCreateDto createDto);

    CommentFullDto update(CommentUpdateDto updateDto);

    void delete(Long id);

    void deleteAllForTrainer(final Long trainerId);
}
