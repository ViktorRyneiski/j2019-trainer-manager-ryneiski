package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.service.AppointmentService;
import by.itstep.trainer.manager.service.AuthService;
import by.itstep.trainer.manager.service.CommentService;
import by.itstep.trainer.manager.service.TrainerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class TrainerController {

    private final AppointmentService appointmentService;
    private final TrainerService trainerService;
    private final CommentService commentService;
    private final AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {

        List<TrainerPreviewDto> found = trainerService.findAll();
        model.addAttribute("trainers", found);
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());

        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/{id}")
    public String openTrainerProfile(@PathVariable Long id, Model model) {
        TrainerFullDto found = trainerService.findById(id);
        model.addAttribute("trainer", found);
        model.addAttribute("comments", found.getComments());
        model.addAttribute("appointments", found.getAppointments());
        model.addAttribute("comment", new CommentCreateDto());
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());
        return "trainer";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/open-create-form")
    public String openSaveTrainer(Model model) {
        model.addAttribute("trainer", new TrainerCreateDto());
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());
        return "create-trainer";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainer/create")
    public String createTrainer(TrainerCreateDto trainer) {
        trainerService.create(trainer);
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/open-update-form/{trainerId}")
    public String openUpdateTrainer(@PathVariable Long trainerId, Model model) {
        model.addAttribute("existingTrainer", trainerService.findById(trainerId));
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());
        return "update-trainer";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/trainer/update")
    public String updateTrainer(TrainerUpdateDto updateDto) {
        trainerService.update(updateDto);
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/trainer/delete/{trainerId}")
    public String deleteTrainer(@PathVariable Long trainerId) {

        appointmentService.deleteAllForTrainer(trainerId);
        commentService.deleteAllForTrainer(trainerId);
        trainerService.delete(trainerId);

        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/comment/open-create-form/{trainerId}")
    public String openSaveComment(@PathVariable Long trainerId, Model model) {
        TrainerFullDto foundTrainer = trainerService.findById(trainerId);
        model.addAttribute("comment", new CommentCreateDto());
        model.addAttribute("trainer", foundTrainer);
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());
        return "create-comment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/comment/save-comment")
    public String saveComment(CommentCreateDto comment) {
        commentService.create(comment);
        return "redirect:/index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/comment/delete/{commentId}")
    public String deleteComment(@PathVariable Long commentId) {
        Long trainerId = commentService.findById(commentId).getTrainer().getId();
        commentService.delete(commentId);

        return "redirect:/trainer/" + trainerId;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/appointment/open-create-form/{trainerId}")
    public String openCreatePage(@PathVariable Long trainerId, Model model) {
        TrainerFullDto found = trainerService.findById(trainerId);
        model.addAttribute("trainer", found);
        model.addAttribute("appointment", new AppointmentCreateDto());
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        model.addAttribute("admin", authService.getLoginedAdmin());

        return "create-appointment";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/appointment/create")
    public String createAppointment(AppointmentCreateDto createDto) {

        appointmentService.create(createDto);

        return "redirect:/trainer/" + createDto.getTrainerId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/appointment/delete/{appointmentId}")
    public String deleteAppointment(@PathVariable Long appointmentId) {
        Long trainerId = appointmentService.findById(appointmentId).getTrainer().getId();
        appointmentService.delete(appointmentId);

        return "redirect:/trainer/" + trainerId;
    }
}
