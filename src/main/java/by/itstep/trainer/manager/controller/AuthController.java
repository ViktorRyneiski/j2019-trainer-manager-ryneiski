package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.dto.admin.AdminLoginDto;
import by.itstep.trainer.manager.service.AdminService;
import by.itstep.trainer.manager.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class AuthController {

    private final AdminService adminService;

    private final AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String openRegistrationPage(Model model) {
        model.addAttribute("createDto", new AdminCreateDto());
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        return "auth/registration";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/sign_in")
    public String openLoginPage(Model model) {
        model.addAttribute("loginDto", new AdminLoginDto());
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        return "auth/sign-in";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin-dto/create")
    public String createAdmin(AdminCreateDto createDto) {
        adminService.create(createDto);
        return "redirect:/registration";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/admin/sign_in")
    public String login(AdminLoginDto loginDto) {
        authService.login(loginDto); // Если логин и пароль подошли, то он будет залогинен

        return "redirect:/index";
//        return "redirect:/profile/" + authService.getLoginedAdmin().getId(); //TODO
    }

    @RequestMapping(method = RequestMethod.GET, value = "/logout")
    public String logout() {
        authService.logout();
        return "redirect:/index";
    }
}
