package by.itstep.trainer.manager.controller;

import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.service.AdminService;
import by.itstep.trainer.manager.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class AdminController {

    private final AdminService adminService;

    private final AuthService authService;

    @RequestMapping(method = RequestMethod.GET, value = "/admin/{id}")
    public String openTrainerProfile(@PathVariable Long id, Model model) {
        AdminFullDto found = adminService.findById(id);
        model.addAttribute("admin", found);
        model.addAttribute("logined", authService.getLoginedAdmin() != null);
        return "admin-profile";
    }
}
