package by.itstep.trainer.manager.repository.impl;

import by.itstep.trainer.manager.entity.Admin;
import by.itstep.trainer.manager.repository.AdminRepository;
import by.itstep.trainer.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class AdminRepositoryImpl implements AdminRepository {

    @Override
    public List<Admin> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Admin> foundAdmins =
                em.createNativeQuery("SELECT * FROM admin", Admin.class)
                        .getResultList();

        em.close();
        return foundAdmins;
    }

    @Override
    public Admin findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Admin foundAdmin = em.find(Admin.class, id);

        em.close();
        return foundAdmin;
    }


    @Override
    public Admin findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        Admin foundAdmin = (Admin) em.createNativeQuery(
                String.format("SELECT * FROM admin WHERE email=\"%s\"",email),
                Admin.class).getSingleResult();
        em.close();
        System.out.println("Found admin: " + foundAdmin);
        return foundAdmin;
    }

    @Override
    public Admin create(final Admin admin) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(admin);

        em.getTransaction().commit();
        em.close();
        return admin;
    }

    @Override
    public Admin update(final Admin admin) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(admin);

        em.getTransaction().commit();
        em.close();
        return admin;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Admin toDelete = em.find(Admin.class, id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM admin").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
