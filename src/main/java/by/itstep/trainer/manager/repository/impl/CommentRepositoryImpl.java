package by.itstep.trainer.manager.repository.impl;

import by.itstep.trainer.manager.entity.Comment;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundComments =
                em.createNativeQuery("SELECT * FROM comment", Comment.class)
                        .getResultList();

        em.close();
        return foundComments;
    }

    @Override
    public Comment findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Comment foundComment = em.find(Comment.class, id);

        em.close();
        return foundComment;
    }

    @Override
    public Comment create(final Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(comment);

        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public Comment update(final Comment comment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(comment);

        em.getTransaction().commit();
        em.close();
        return comment;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Comment toDelete = em.find(Comment.class, id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM comment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
