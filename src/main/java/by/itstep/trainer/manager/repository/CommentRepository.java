package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> findAll();

    Comment findById(Long id);

    Comment create(Comment comment);

    Comment update(Comment comment);

    void delete(Long id);

    void deleteAll();
}
