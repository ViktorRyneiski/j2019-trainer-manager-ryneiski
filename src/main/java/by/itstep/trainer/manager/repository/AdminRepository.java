package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Admin;

import java.util.List;

public interface AdminRepository {

    List<Admin> findAll();

    Admin findById(Long id);

    Admin findByEmail(String email);

    Admin create(Admin admin);

    Admin update(Admin admin);

    void delete(Long id);

    void deleteAll();
}
