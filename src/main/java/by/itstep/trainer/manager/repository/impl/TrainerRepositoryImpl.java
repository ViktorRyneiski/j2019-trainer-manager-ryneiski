package by.itstep.trainer.manager.repository.impl;

import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class TrainerRepositoryImpl implements TrainerRepository {

    @Override
    public List<Trainer> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Trainer> foundTrainers =
                em.createNativeQuery("SELECT * FROM trainer", Trainer.class)
                        .getResultList();

        em.close();
        return foundTrainers;
    }

    @Override
    public Trainer findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Trainer foundTrainer = em.find(Trainer.class, id);
        if (foundTrainer == null) {
            return null;
        }

        Hibernate.initialize(foundTrainer.getAppointments());
        Hibernate.initialize(foundTrainer.getComments());

        em.close();
        return foundTrainer;
    }

    @Override
    public Trainer create(final Trainer trainer) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public Trainer update(final Trainer trainer) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(trainer);

        em.getTransaction().commit();
        em.close();
        return trainer;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Trainer toDelete = em.find(Trainer.class, id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM trainer").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
