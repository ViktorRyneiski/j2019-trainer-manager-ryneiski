package by.itstep.trainer.manager.repository.impl;

import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import javax.persistence.EntityManager;

@Repository
public class AppointmentRepositoryImpl implements AppointmentRepository {

    @Override
    public List<Appointment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Appointment> foundAppointments =
                em.createNativeQuery("SELECT * FROM appointment", Appointment.class)
                        .getResultList();

        em.close();
        return foundAppointments;
    }

    @Override
    public Appointment findById(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        Appointment foundAppointment = em.find(Appointment.class, id);

        em.close();
        return foundAppointment;
    }

    @Override
    public Appointment create(final Appointment appointment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(appointment);

        em.getTransaction().commit();
        em.close();
        return appointment;
    }

    @Override
    public Appointment update(final Appointment appointment) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.merge(appointment);

        em.getTransaction().commit();
        em.close();
        return appointment;
    }

    @Override
    public void delete(final Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        Appointment toDelete = em.find(Appointment.class, id);
        em.remove(toDelete);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM appointment").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
