package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Trainer;

import java.util.List;

public interface TrainerRepository {

    List<Trainer> findAll();

    Trainer findById(Long id);

    Trainer create(Trainer trainer);

    Trainer update(Trainer trainer);

    void delete(Long id);

    void deleteAll();
}
