package by.itstep.trainer.manager.repository;

import by.itstep.trainer.manager.entity.Appointment;

import java.util.List;

public interface AppointmentRepository {

    List<Appointment> findAll();

    Appointment findById(Long id);

    Appointment create(Appointment appointment);

    Appointment update(Appointment appointment);

    void delete(Long id);

    void deleteAll();
}
