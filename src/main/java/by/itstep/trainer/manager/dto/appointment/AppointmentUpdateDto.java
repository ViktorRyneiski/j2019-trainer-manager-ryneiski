package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

@Data
public class AppointmentUpdateDto {

    private Long id;
    private String message;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String phone;
}
