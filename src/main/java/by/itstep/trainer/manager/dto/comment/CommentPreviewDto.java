package by.itstep.trainer.manager.dto.comment;

import lombok.Data;

@Data
public class CommentPreviewDto {

    private Long id;
    private String message;
    private String userFirstName;
    private String userLastName;
}
