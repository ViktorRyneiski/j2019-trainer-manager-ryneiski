package by.itstep.trainer.manager.dto.comment;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

@Data
public class CommentFullDto {

    private Long id;
    private String message;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private int markToTheTrainer;
    private boolean published;
    private Trainer trainer;
}
