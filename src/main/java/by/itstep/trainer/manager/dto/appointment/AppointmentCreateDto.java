package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AppointmentCreateDto {

    private String message;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String phone;
    private Long trainerId;
    private Timestamp createdAt;
}
