package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerPreviewDto {

    private Long id;
    private String name;
    private int experience;
    private String avatarUrl;
    private String descriptionOfAchievements;

}
