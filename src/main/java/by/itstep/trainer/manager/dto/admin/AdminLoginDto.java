package by.itstep.trainer.manager.dto.admin;

import lombok.Data;

@Data
public class AdminLoginDto {

    private String email;
    private String password;
}
