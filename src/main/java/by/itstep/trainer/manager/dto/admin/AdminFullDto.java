package by.itstep.trainer.manager.dto.admin;

import by.itstep.trainer.manager.entity.enums.Role;
import lombok.Data;

@Data
public class AdminFullDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Role role;
}
