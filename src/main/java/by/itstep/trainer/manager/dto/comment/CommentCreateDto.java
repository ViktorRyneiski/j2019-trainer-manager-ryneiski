package by.itstep.trainer.manager.dto.comment;

import lombok.Data;

@Data
public class CommentCreateDto {

    private String message;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private int markToTheTrainer;
    private Long trainerId;
}
