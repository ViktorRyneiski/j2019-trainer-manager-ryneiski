package by.itstep.trainer.manager.dto.appointment;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class AppointmentPreviewDto {

    private Long id;
    private String userFirstName;
    private String userLastName;
    private String trainerInfo;
    private Timestamp createdAt;
}
