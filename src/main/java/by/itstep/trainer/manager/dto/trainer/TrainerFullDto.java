package by.itstep.trainer.manager.dto.trainer;

import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.entity.Comment;
import lombok.Data;

import java.util.List;

@Data
public class TrainerFullDto {

    private Long id;
    private String firstName;
    private String lastName;
    private int experience;
    private String descriptionOfAchievements;
    private String avatarUrl;
    private String email;
    private String password;
    private List<Comment> comments;
    private List<Appointment> appointments;

}
