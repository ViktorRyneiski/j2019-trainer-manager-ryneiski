package by.itstep.trainer.manager.dto.admin;

import lombok.Data;

@Data
public class AdminCreateDto {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
