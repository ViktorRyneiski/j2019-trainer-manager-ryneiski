package by.itstep.trainer.manager.dto.appointment;

import by.itstep.trainer.manager.entity.Trainer;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class AppointmentFullDto {

    private Long id;
    private Timestamp createdAt;
    private String message;
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String phone;
    private Trainer trainer;
}
