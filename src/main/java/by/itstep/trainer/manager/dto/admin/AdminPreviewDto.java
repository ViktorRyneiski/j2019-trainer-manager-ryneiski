package by.itstep.trainer.manager.dto.admin;

import lombok.Data;

@Data
public class AdminPreviewDto {

    private Long id;
    private String firstName;
    private String lastName;
}
