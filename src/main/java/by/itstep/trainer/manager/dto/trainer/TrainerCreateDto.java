package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerCreateDto {

    private String firstName;
    private String lastName;
    private int experience;
    private String descriptionOfAchievements;
    private String avatarUrl;
    private String email;
    private String password;
}
