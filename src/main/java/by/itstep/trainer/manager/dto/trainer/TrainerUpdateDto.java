package by.itstep.trainer.manager.dto.trainer;

import lombok.Data;

@Data
public class TrainerUpdateDto {

    private Long id;
    private String firstName;
    private String lastName;
    private int experience;
    private String descriptionOfAchievements;
    private String avatarUrl;
    private String email;
}
