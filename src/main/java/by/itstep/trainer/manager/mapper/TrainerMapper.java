package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainerMapper {

    public List<TrainerPreviewDto> mapToDtoList(List<Trainer> entities) {
        List<TrainerPreviewDto> dtos = new ArrayList<>();

        for (Trainer entity : entities) {
            dtos.add(mapToPreviewDto(entity));
        }
        return dtos;
    }

    public Trainer mapToEntity(TrainerCreateDto createDto) {
        Trainer entity = new Trainer();
        entity.setFirstName(createDto.getFirstName());
        entity.setLastName(createDto.getLastName());
        entity.setAvatarUrl(createDto.getAvatarUrl());
        entity.setDescriptionOfAchievements(createDto.getDescriptionOfAchievements());
        entity.setEmail(createDto.getEmail());
        entity.setPassword(createDto.getPassword());
        entity.setExperience(createDto.getExperience());

        return entity;
    }

    public Trainer mapToEntity(TrainerUpdateDto updateDto) {
        Trainer entity = new Trainer();
        entity.setId(updateDto.getId());
        entity.setFirstName(updateDto.getFirstName());
        entity.setLastName(updateDto.getLastName());
        entity.setAvatarUrl(updateDto.getAvatarUrl());
        entity.setDescriptionOfAchievements(updateDto.getDescriptionOfAchievements());
        entity.setEmail(updateDto.getEmail());
        entity.setExperience(updateDto.getExperience());

        return entity;
    }

    public TrainerFullDto mapToDto(Trainer entity) {
        if (entity == null) {
            return null;
        }
        TrainerFullDto dto = new TrainerFullDto();
        dto.setId(entity.getId());
        dto.setAppointments(entity.getAppointments());
        dto.setAvatarUrl(entity.getAvatarUrl());
        dto.setComments(entity.getComments());
        dto.setDescriptionOfAchievements(entity.getDescriptionOfAchievements());
        dto.setEmail(entity.getEmail());
        dto.setExperience(entity.getExperience());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPassword(entity.getPassword());

        return dto;
    }

    public TrainerPreviewDto mapToPreviewDto(Trainer entity) {
        TrainerPreviewDto dto = new TrainerPreviewDto();
        dto.setId(entity.getId());
        dto.setAvatarUrl(entity.getAvatarUrl());
        dto.setExperience(entity.getExperience());
        dto.setDescriptionOfAchievements(entity.getDescriptionOfAchievements());
        dto.setName(entity.getFirstName() + " " + entity.getLastName());
        return dto;
    }
}
