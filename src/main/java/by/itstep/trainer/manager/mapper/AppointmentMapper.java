package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.entity.Trainer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppointmentMapper {

    public List<AppointmentPreviewDto> mapToDtoList(List<Appointment> entities) {
        List<AppointmentPreviewDto> dtos = new ArrayList<>();

        for (Appointment entity : entities) {
            dtos.add(mapToPreviewDto(entity));
        }
        return dtos;
    }

    public Appointment mapToEntity(AppointmentCreateDto createDto, Trainer trainer) {
        Appointment entity = new Appointment();
        entity.setUserFirstName(createDto.getUserFirstName());
        entity.setUserLastName(createDto.getUserLastName());
        entity.setUserEmail(createDto.getUserEmail());
        entity.setMessage(createDto.getMessage());
        entity.setPhone(createDto.getPhone());
        entity.setTrainer(trainer);
        entity.setCreatedAt(createDto.getCreatedAt());

        return entity;
    }

    public Appointment mapToEntity(AppointmentUpdateDto updateDto) {
        Appointment entity = new Appointment();
        entity.setUserFirstName(updateDto.getUserFirstName());
        entity.setUserLastName(updateDto.getUserLastName());
        entity.setUserEmail(updateDto.getUserEmail());
        entity.setMessage(updateDto.getMessage());
        entity.setPhone(updateDto.getPhone());
        entity.setId(updateDto.getId());

        return entity;
    }

    public AppointmentFullDto mapToDto(Appointment entity) {

        if (entity == null) {
            return null;
        }
        AppointmentFullDto dto = new AppointmentFullDto();
        dto.setId(entity.getId());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setMessage(entity.getMessage());
        dto.setPhone(entity.getPhone());
        dto.setUserEmail(entity.getUserEmail());
        dto.setTrainer(entity.getTrainer());
        dto.setUserFirstName(entity.getUserFirstName());
        dto.setUserLastName(entity.getUserLastName());

        return dto;
    }

    public AppointmentPreviewDto mapToPreviewDto(Appointment entity) {
        AppointmentPreviewDto dto = new AppointmentPreviewDto();
        dto.setId(entity.getId());
        dto.setUserFirstName(entity.getUserFirstName());
        dto.setUserLastName(entity.getUserLastName());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setTrainerInfo(entity.getTrainer().getFirstName() +
                " " + entity.getTrainer().getLastName());
        return dto;
    }
}
