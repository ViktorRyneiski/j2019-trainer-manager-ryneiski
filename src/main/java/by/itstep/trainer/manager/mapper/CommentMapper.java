package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.Comment;
import by.itstep.trainer.manager.entity.Trainer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentMapper {

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities) {

        List<CommentPreviewDto> dtos = new ArrayList<>();

        for (Comment entity : entities) {
            dtos.add(mapToPreviewDto(entity));
        }
        return dtos;
    }

    public Comment mapToEntity(CommentCreateDto createDto, Trainer trainer) {
        Comment entity = new Comment();
        entity.setUserFirstName(createDto.getUserFirstName());
        entity.setUserLastName(createDto.getUserLastName());
        entity.setUserEmail(createDto.getUserEmail());
        entity.setMessage(createDto.getMessage());
        entity.setMarkToTheTrainer(createDto.getMarkToTheTrainer());
        entity.setTrainer(trainer);

        return entity;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto) {
        Comment entity = new Comment();
        entity.setId(updateDto.getId());
        entity.setMessage(updateDto.getMessage());
        entity.setMarkToTheTrainer(updateDto.getMarkToTheTrainer());

        return entity;
    }

    public CommentFullDto mapToDto(Comment entity) {
        if (entity == null) {
            return null;
        }
        CommentFullDto dto = new CommentFullDto();
        dto.setId(entity.getId());
        dto.setMessage(entity.getMessage());
        dto.setUserFirstName(entity.getUserFirstName());
        dto.setUserLastName(entity.getUserLastName());
        dto.setUserEmail(entity.getUserEmail());
        dto.setMarkToTheTrainer(entity.getMarkToTheTrainer());
        dto.setPublished(entity.isPublished());
        dto.setTrainer(entity.getTrainer());

        return dto;
    }

    public CommentPreviewDto mapToPreviewDto(Comment entity) {
        CommentPreviewDto dto = new CommentPreviewDto();
        dto.setId(entity.getId());
        dto.setMessage(entity.getMessage());
        dto.setUserFirstName(entity.getUserFirstName());
        dto.setUserLastName(entity.getUserLastName());
        return dto;
    }
}
