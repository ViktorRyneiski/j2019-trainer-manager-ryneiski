package by.itstep.trainer.manager.mapper;

import by.itstep.trainer.manager.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.dto.admin.AdminUpdateDto;
import by.itstep.trainer.manager.entity.Admin;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminMapper {

    public List<AdminPreviewDto> mapToDtoList(List<Admin> entities) {
        List<AdminPreviewDto> dtos = new ArrayList<>();

        for (Admin entity : entities) {
            dtos.add(mapToPreviewDto(entity));
        }
        return dtos;
    }

    public Admin mapToEntity(AdminCreateDto createDto) {
        Admin entity = new Admin();
        entity.setEmail(createDto.getEmail());
        entity.setFirstName(createDto.getFirstName());
        entity.setLastName(createDto.getLastName());
        entity.setPassword(createDto.getPassword());

        return entity;
    }

    public Admin mapToEntity(AdminUpdateDto updateDto) {
        Admin entity = new Admin();
        entity.setId(updateDto.getId());
        entity.setFirstName(updateDto.getFirstName());
        entity.setLastName(updateDto.getLastName());

        return entity;
    }

    public AdminFullDto mapToDto(Admin entity) {
        if (entity == null) {
            return null;
        }
        AdminFullDto dto = new AdminFullDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPassword(entity.getPassword());
        dto.setRole(entity.getRole());

        return dto;
    }

    public AdminPreviewDto mapToPreviewDto(Admin entity) {
        AdminPreviewDto dto = new AdminPreviewDto();
        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());

        return dto;
    }
}
