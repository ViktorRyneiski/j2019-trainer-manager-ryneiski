package by.itstep.trainer.manager.appointment;

import by.itstep.trainer.manager.dto.appointment.AppointmentCreateDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentFullDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentPreviewDto;
import by.itstep.trainer.manager.dto.appointment.AppointmentUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.AppointmentMapper;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.impl.AppointmentRepositoryImpl;
import by.itstep.trainer.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainer.manager.service.AppointmentService;
import by.itstep.trainer.manager.service.impl.AppointmentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.List;

@SpringBootTest
public class AppointmentServiceTest {

    private final AppointmentService service = new AppointmentServiceImpl(new AppointmentRepositoryImpl(), new AppointmentMapper(), new TrainerRepositoryImpl());
    private final AppointmentRepository repository = new AppointmentRepositoryImpl();

    private final TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void getById_happyPath() {
        //given
        AppointmentCreateDto appointmentCreateDto = getAppointmentCreateDto();

        //when
        AppointmentFullDto saved = service.create(appointmentCreateDto);
        AppointmentFullDto toTest = service.findById(saved.getId());

        //then
        Assertions.assertNotNull(toTest);
    }

    @Test
    void testFindAll_happyPath() {
        //given
        for (int i = 0; i < 5; i++) {
            service.create(getAppointmentCreateDto());
        }

        //when

        List<AppointmentPreviewDto> found = service.findAll();

        //then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        AppointmentCreateDto appointmentCreateDto = getAppointmentCreateDto();

        //when
        AppointmentFullDto saved = service.create(appointmentCreateDto);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        AppointmentCreateDto toCreate = getAppointmentCreateDto();

        //when
        AppointmentFullDto saved = service.create(toCreate);

        AppointmentUpdateDto updateDto = new AppointmentUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setUserFirstName(saved.getUserFirstName());
        updateDto.setUserLastName("new");
        updateDto.setUserEmail(saved.getUserEmail());
        updateDto.setMessage(saved.getMessage());
        updateDto.setPhone(saved.getPhone());

        AppointmentFullDto updated = service.update(updateDto);

        //then
        Assertions.assertEquals(updateDto.getUserLastName(), updated.getUserLastName());
    }

    @Test
    void testDelete_happyPath() {

        //given
        AppointmentCreateDto toCreate = getAppointmentCreateDto();
        AppointmentFullDto saved = service.create(toCreate);

        //when
        service.delete(saved.getId());

        AppointmentFullDto toCheck = service.findById(saved.getId());

        //then
        Assertions.assertNull(toCheck);
    }

    private AppointmentCreateDto getAppointmentCreateDto() {
        Trainer trainer = Trainer.builder()
                .password("sfgwfwsf")
                .email("Gizzzer@tut.by" + Math.random() * 1000)
                .firstName("Maestro")
                .lastName("Ardi")
                .build();
        Long trainerId = trainerRepository
                .create(trainer)
                .getId();

        AppointmentCreateDto appointmentCreateDto = new AppointmentCreateDto();
        appointmentCreateDto.setUserFirstName("Viktor");
        appointmentCreateDto.setUserLastName("Ryneiski");
        appointmentCreateDto.setUserEmail("Gizzzer@tut.by" + Math.random() * 1000);
        appointmentCreateDto.setMessage("asdd");
        appointmentCreateDto.setPhone("121287");
        appointmentCreateDto.setTrainerId(trainerId);
        appointmentCreateDto.setCreatedAt(new Timestamp(2000 - 10 - 10));
        return appointmentCreateDto;
    }
}
