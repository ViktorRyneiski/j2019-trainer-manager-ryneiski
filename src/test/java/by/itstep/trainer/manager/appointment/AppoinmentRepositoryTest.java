package by.itstep.trainer.manager.appointment;

import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.impl.AppointmentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class AppoinmentRepositoryTest {

    private final AppointmentRepository repository = new AppointmentRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void getById_HappyPath() {
        //given
        Appointment appointment = getAppointment();

        //when
        Appointment saved = repository.create(appointment);
        Appointment toEquals = repository.findById(saved.getId());

        //then
        Assertions.assertEquals(appointment.getClass(), toEquals.getClass());
    }

    @Test
    void testfindAll_HappyPath() {
        //given
        List<Appointment> appointments = getListAppointments();

        for (Appointment appointment : appointments) {
            repository.create(appointment);
        }

        //when

        List<Appointment> found = repository.findAll();

        //then
        Assertions.assertEquals(appointments.size(), found.size());
    }

    @Test
    void testCreate_HappyPath() {
        //given
        Appointment appointment = getAppointment();

        //when
        Appointment saved = repository.create(appointment);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_HappyPath() {
        //given
        Appointment appointment = getAppointment();

        //when
        Appointment saved = repository.create(appointment);

        Appointment toUpdate = repository.findById(saved.getId());
        toUpdate.setUserEmail("new");

        Appointment updated = repository.update(toUpdate);

        //then
        Assertions.assertEquals(toUpdate.getUserEmail(), updated.getUserEmail());
    }

    @Test
    void testDelete_HappyPath() {

        //given
        Appointment appointment = getAppointment();

        //when
        Appointment saved = repository.create(appointment);
        repository.delete(saved.getId());

        Appointment toEquals = repository.findById(saved.getId());

        //then
        Assertions.assertEquals(null, toEquals);
    }

    @Test
    void testDeleteAll_HappyPath() {
        //given
        List<Appointment> appointments = getListAppointments();

        //when
        for (Appointment toSave : appointments) {
            repository.create(toSave);
        }
        repository.deleteAll();

        List<Appointment> appointmentsDeleted = repository.findAll();

        //then
        Assertions.assertEquals(0, appointmentsDeleted.size());
    }

    private Appointment getAppointment() {
        Appointment appointment = Appointment.builder()
                .userFirstName("Fiodor")
                .userLastName("Fiodorovich")
                .createdAt(new Timestamp(2000 - 12 - 12))
                .userEmail("Fiodor@mail.ru" + Math.random() * 1000)
                .message("Hello people")
                .phone("80298745621")
                .build();
        return appointment;
    }

    private List<Appointment> getListAppointments() {
        List<Appointment> appointments = new ArrayList<>();
        appointments.add(getAppointment());
        appointments.add(getAppointment());
        appointments.add(getAppointment());
        appointments.add(getAppointment());
        appointments.add(getAppointment());
        return appointments;
    }
}
