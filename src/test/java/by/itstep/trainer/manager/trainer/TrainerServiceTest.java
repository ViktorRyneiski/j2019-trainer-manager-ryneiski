package by.itstep.trainer.manager.trainer;

import by.itstep.trainer.manager.dto.trainer.TrainerCreateDto;
import by.itstep.trainer.manager.dto.trainer.TrainerFullDto;
import by.itstep.trainer.manager.dto.trainer.TrainerPreviewDto;
import by.itstep.trainer.manager.dto.trainer.TrainerUpdateDto;
import by.itstep.trainer.manager.mapper.TrainerMapper;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.trainer.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainer.manager.service.TrainerService;
import by.itstep.trainer.manager.service.impl.TrainerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TrainerServiceTest {

    private final TrainerService service = new TrainerServiceImpl(new TrainerRepositoryImpl(), new TrainerMapper());
    private final TrainerRepository repository = new TrainerRepositoryImpl();

    private final CommentRepository commentRepository = new CommentRepositoryImpl();

    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
        repository.deleteAll();
    }

    @Test
    void getById_happyPath() {
        //given
        TrainerCreateDto trainerCreateDto = getTrainerCreateDto();

        //when
        TrainerFullDto saved = service.create(trainerCreateDto);
        TrainerFullDto toTest = service.findById(saved.getId());

        //then
        Assertions.assertNotNull(toTest);
    }

    @Test
    void testFindAll_happyPath() {
        //given
        for (int i = 0; i < 5; i++) {
            service.create(getTrainerCreateDto());
        }

        //when

        List<TrainerPreviewDto> found = service.findAll();

        //then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        TrainerCreateDto trainerCreateDto = getTrainerCreateDto();

        //when
        TrainerFullDto saved = service.create(trainerCreateDto);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        TrainerCreateDto toCreate = getTrainerCreateDto();

        //when
        TrainerFullDto saved = service.create(toCreate);

        TrainerUpdateDto updateDto = new TrainerUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setFirstName(saved.getFirstName());
        updateDto.setLastName("new");
        updateDto.setEmail(saved.getEmail());

        TrainerFullDto updated = service.update(updateDto);

        //then
        Assertions.assertEquals(updateDto.getLastName(), updated.getLastName());
    }

    @Test
    void testDelete_happyPath() {

        //given
        TrainerCreateDto toCreate = getTrainerCreateDto();
        TrainerFullDto saved = service.create(toCreate);

        //when
        service.delete(saved.getId());

        TrainerFullDto toCheck = service.findById(saved.getId());

        //then
        Assertions.assertNull(toCheck);
    }

    private TrainerCreateDto getTrainerCreateDto() {
        TrainerCreateDto trainerCreateDto = new TrainerCreateDto();
        trainerCreateDto.setFirstName("Viktor");
        trainerCreateDto.setLastName("Ryneiski");
        trainerCreateDto.setEmail("Gizzzer@tut.by" + Math.random() * 1000);
        trainerCreateDto.setPassword("121287");
        return trainerCreateDto;
    }
}
