package by.itstep.trainer.manager.trainer;

import by.itstep.trainer.manager.entity.Appointment;
import by.itstep.trainer.manager.entity.Comment;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.repository.AppointmentRepository;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.impl.AppointmentRepositoryImpl;
import by.itstep.trainer.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.trainer.manager.repository.impl.TrainerRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TrainerRepositoryTest {

    private final TrainerRepository trainerRepository = new TrainerRepositoryImpl();
    private final AppointmentRepository appointmentRepository = new AppointmentRepositoryImpl();
    private final CommentRepository commentRepository = new CommentRepositoryImpl();

    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
        appointmentRepository.deleteAll();
        trainerRepository.deleteAll();
    }

    @Test
    void getById_HappyPath() {
        //given
        Trainer trainer = getRandomTrainer();

        Trainer saved = trainerRepository.create(trainer);

        Comment c1 = Comment.builder()
                .userFirstName("Fory")
                .userLastName("Magov")
                .userEmail("magov@mail.ru")
                .message("c1")
                .trainer(saved)
                .build();

        Comment c2 = Comment.builder()
                .userFirstName("Egor")
                .userLastName("Kartavy")
                .userEmail("Yaga777@tut.by")
                .message("c2")
                .trainer(saved)
                .build();

        Appointment a1 = Appointment.builder()
                .message("1")
                .userFirstName("Jora")
                .userLastName("Kotov")
                .userEmail("a1")
                .trainer(saved)
                .createdAt(new Timestamp(2000 - 12 - 12))
                .phone("6464644654")
                .build();

        Appointment a2 = Appointment.builder()
                .message("2")
                .userFirstName("Kira")
                .userLastName("Martynova")
                .userEmail("a2")
                .trainer(saved)
                .createdAt(new Timestamp(2000 - 10 - 12))
                .phone("8448948944")
                .build();

        commentRepository.create(c1);
        commentRepository.create(c2);

        appointmentRepository.create(a1);
        appointmentRepository.create(a2);

        //when
        Trainer found = trainerRepository.findById(saved.getId());

        //then
        Assertions.assertNotNull(found);
        Assertions.assertNotNull(found.getId());
        Assertions.assertNotNull(found.getAppointments());
        Assertions.assertNotNull(found.getComments());
        Assertions.assertEquals(2, found.getAppointments().size());
        Assertions.assertEquals(2, found.getComments().size());
    }

    @Test
    void testfindAll_HappyPath() {
        //given
        List<Trainer> trainers = getListTrainers();

        for (Trainer trainer : trainers) {
            trainerRepository.create(trainer);
        }

        //when

        List<Trainer> found = trainerRepository.findAll();

        //then
        Assertions.assertEquals(trainers.size(), found.size());
    }

    @Test
    void testCreate_HappyPath() {
        //given
        Trainer trainer = getRandomTrainer();

        //when
        Trainer saved = trainerRepository.create(trainer);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_HappyPath() {
        //given
        Trainer trainer = getRandomTrainer();

        //when
        Trainer saved = trainerRepository.create(trainer);

        Trainer toUpdate = trainerRepository.findById(saved.getId());
        toUpdate.setEmail("new");

        Trainer updated = trainerRepository.update(toUpdate);

        //then
        Assertions.assertEquals(toUpdate.getEmail(), updated.getEmail());
    }

    @Test
    void testDelete_HappyPath() {

        //given
        Trainer trainer = getRandomTrainer();

        //when
        Trainer saved = trainerRepository.create(trainer);
        trainerRepository.delete(saved.getId());

        List<Trainer> allTrainers = trainerRepository.findAll();

        //then
        Assertions.assertFalse(allTrainers.contains(saved));
    }

    @Test
    void testDeleteAll_HappyPath() {
        //given
        List<Trainer> trainers = getListTrainers();

        //when
        for (Trainer toSave : trainers) {
            trainerRepository.create(toSave);
        }
        trainerRepository.deleteAll();

        List<Trainer> adminsDeleted = trainerRepository.findAll();

        //then
        Assertions.assertEquals(0, adminsDeleted.size());
    }

    private Trainer getRandomTrainer() {
        Trainer trainer = Trainer.builder()
                .firstName("Trainer" + Math.random() * 1000)
                .lastName("x")
                .password("864564564654")
                .email("Trainer@gmail.com" + Math.random() * 1000)
                .experience((int) (Math.random() * 20))
                .descriptionOfAchievements("Well")
                .avatarUrl("http://.....")
                .build();

        return trainer;
    }

    private List<Trainer> getListTrainers() {
        List<Trainer> trainers = new ArrayList<>();
        trainers.add(getRandomTrainer());
        trainers.add(getRandomTrainer());
        trainers.add(getRandomTrainer());
        trainers.add(getRandomTrainer());
        trainers.add(getRandomTrainer());
        return trainers;
    }
}
