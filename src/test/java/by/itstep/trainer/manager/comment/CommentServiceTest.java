package by.itstep.trainer.manager.comment;

import by.itstep.trainer.manager.dto.comment.CommentCreateDto;
import by.itstep.trainer.manager.dto.comment.CommentFullDto;
import by.itstep.trainer.manager.dto.comment.CommentPreviewDto;
import by.itstep.trainer.manager.dto.comment.CommentUpdateDto;
import by.itstep.trainer.manager.entity.Trainer;
import by.itstep.trainer.manager.mapper.CommentMapper;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.TrainerRepository;
import by.itstep.trainer.manager.repository.impl.CommentRepositoryImpl;
import by.itstep.trainer.manager.repository.impl.TrainerRepositoryImpl;
import by.itstep.trainer.manager.service.CommentService;
import by.itstep.trainer.manager.service.impl.CommentServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentServiceTest {

    private final CommentService service = new CommentServiceImpl(new CommentRepositoryImpl(), new CommentMapper(), new TrainerRepositoryImpl());
    private final CommentRepository repository = new CommentRepositoryImpl();
    private final TrainerRepository trainerRepository = new TrainerRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void getById_happyPath() {
        //given
        CommentCreateDto commentCreateDto = getCommentCreateDto();

        //when
        CommentFullDto saved = service.create(commentCreateDto);
        CommentFullDto toTest = service.findById(saved.getId());

        //then
        Assertions.assertNotNull(toTest);
    }

    @Test
    void testFindAll_happyPath() {
        //given
        for (int i = 0; i < 5; i++) {
            service.create(getCommentCreateDto());
        }

        //when

        List<CommentPreviewDto> found = service.findAll();

        //then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        CommentCreateDto commentCreateDto = getCommentCreateDto();

        //when
        CommentFullDto saved = service.create(commentCreateDto);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        CommentCreateDto toCreate = getCommentCreateDto();

        //when
        CommentFullDto saved = service.create(toCreate);

        CommentUpdateDto updateDto = new CommentUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setMarkToTheTrainer(saved.getMarkToTheTrainer());
        updateDto.setMessage("new");

        CommentFullDto updated = service.update(updateDto);

        //then
        Assertions.assertEquals(updateDto.getMarkToTheTrainer(), updated.getMarkToTheTrainer());
    }

    @Test
    void testDelete_happyPath() {

        //given
        CommentCreateDto toCreate = getCommentCreateDto();
        CommentFullDto saved = service.create(toCreate);

        //when
        service.delete(saved.getId());

        CommentFullDto toCheck = service.findById(saved.getId());

        //then
        Assertions.assertNull(toCheck);
    }

    private CommentCreateDto getCommentCreateDto() {
        Trainer trainer = Trainer.builder()
                .password("sfgwfwsf")
                .email("Gizzzer@tut.by" + Math.random() * 1000)
                .firstName("Maestro")
                .lastName("Ardi")
                .build();
        Long trainerId = trainerRepository
                .create(trainer)
                .getId();
        CommentCreateDto commentCreateDto = new CommentCreateDto();
        commentCreateDto.setUserFirstName("Viktor");
        commentCreateDto.setUserLastName("Ryneiski");
        commentCreateDto.setUserEmail("Gizzzer@tut.by" + Math.random() * 1000);
        commentCreateDto.setMessage("121287");
        commentCreateDto.setMarkToTheTrainer(7);
        commentCreateDto.setTrainerId(trainerId);
        return commentCreateDto;
    }
}
