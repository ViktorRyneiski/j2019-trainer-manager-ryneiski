package by.itstep.trainer.manager.comment;

import by.itstep.trainer.manager.entity.Comment;
import by.itstep.trainer.manager.repository.CommentRepository;
import by.itstep.trainer.manager.repository.impl.CommentRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class CommentRepositoryTest {

    private final CommentRepository commentRepository = new CommentRepositoryImpl();

    @BeforeEach
    void setUp() {
        commentRepository.deleteAll();
    }

    @Test
    void getById_HappyPath() {
        //given
        Comment comment = getComment();

        //when
        Comment saved = commentRepository.create(comment);
        Comment toEquals = commentRepository.findById(saved.getId());

        //then
        Assertions.assertEquals(comment.getClass(), toEquals.getClass());
    }

    @Test
    void testfindAll_HappyPath() {
        //given
        List<Comment> comments = getListComments();

        for (Comment comment : comments) {
            commentRepository.create(comment);
        }

        //when

        List<Comment> found = commentRepository.findAll();

        //then
        Assertions.assertEquals(comments.size(), found.size());
    }

    @Test
    void testCreate_HappyPath() {
        //given
        Comment comment = getComment();

        //when
        Comment saved = commentRepository.create(comment);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_HappyPath() {
        //given
        Comment comment = getComment();

        //when
        Comment saved = commentRepository.create(comment);

        Comment toUpdate = commentRepository.findById(saved.getId());
        toUpdate.setUserEmail("new");

        Comment updated = commentRepository.update(toUpdate);

        //then
        Assertions.assertEquals(toUpdate.getUserEmail(), updated.getUserEmail());
    }

    @Test
    void testDelete_HappyPath() {

        //given
        Comment comment = getComment();

        //when
        Comment saved = commentRepository.create(comment);
        commentRepository.delete(saved.getId());

        Comment toEquals = commentRepository.findById(saved.getId());

        //then
        Assertions.assertNull(toEquals);
    }

    @Test
    void testDeleteAll_HappyPath() {
        //given
        List<Comment> comments = getListComments();

        //when
        for (Comment toSave : comments) {
            commentRepository.create(toSave);
        }
        commentRepository.deleteAll();

        List<Comment> commentsDeleted = commentRepository.findAll();

        //then
        Assertions.assertEquals(0, commentsDeleted.size());
    }

    private Comment getComment() {
        Comment admin = Comment.builder()
                .userFirstName("Viktor")
                .userLastName("Ryneiski")
                .userEmail("Gizzzer@tut.by" + Math.random() * 1000)
                .message("Good comment")
                .markToTheTrainer((int) (Math.random() * 10))
                .build();
        return admin;
    }

    private List<Comment> getListComments() {
        List<Comment> comments = new ArrayList<>();
        comments.add(getComment());
        comments.add(getComment());
        comments.add(getComment());
        comments.add(getComment());
        comments.add(getComment());
        return comments;
    }
}
