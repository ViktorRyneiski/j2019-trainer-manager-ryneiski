package by.itstep.trainer.manager.admin;

import by.itstep.trainer.manager.dto.admin.AdminCreateDto;
import by.itstep.trainer.manager.dto.admin.AdminFullDto;
import by.itstep.trainer.manager.dto.admin.AdminPreviewDto;
import by.itstep.trainer.manager.dto.admin.AdminUpdateDto;
import by.itstep.trainer.manager.mapper.AdminMapper;
import by.itstep.trainer.manager.repository.AdminRepository;
import by.itstep.trainer.manager.repository.impl.AdminRepositoryImpl;
import by.itstep.trainer.manager.service.AdminService;
import by.itstep.trainer.manager.service.impl.AdminServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminServiceTest {

    private final AdminService service = new AdminServiceImpl(new AdminRepositoryImpl(), new AdminMapper());
    private final AdminRepository repository = new AdminRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void getById_happyPath() {
        //given
        AdminCreateDto adminCreateDto = getAdminCreateDto();

        //when
        AdminFullDto saved = service.create(adminCreateDto);
        AdminFullDto toTest = service.findById(saved.getId());

        //then
        Assertions.assertNotNull(toTest);
    }

    @Test
    void testFindAll_happyPath() {
        //given
        for (int i = 0; i < 5; i++) {
            service.create(getAdminCreateDto());
        }

        //when

        List<AdminPreviewDto> found = service.findAll();

        //then
        Assertions.assertEquals(5, found.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        AdminCreateDto adminCreateDto = getAdminCreateDto();

        //when
        AdminFullDto saved = service.create(adminCreateDto);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        AdminCreateDto toCreate = getAdminCreateDto();

        //when
        AdminFullDto saved = service.create(toCreate);

        AdminUpdateDto updateDto = new AdminUpdateDto();
        updateDto.setId(saved.getId());
        updateDto.setFirstName(saved.getFirstName());
        updateDto.setLastName("new");

        AdminFullDto updated = service.update(updateDto);

        //then
        Assertions.assertEquals(updateDto.getLastName(), updated.getLastName());
    }

    @Test
    void testDelete_happyPath() {

        //given
        AdminCreateDto toCreate = getAdminCreateDto();
        AdminFullDto saved = service.create(toCreate);

        //when
        service.delete(saved.getId());

        AdminFullDto toCheck = service.findById(saved.getId());

        //then
        Assertions.assertNull(toCheck);
    }

    private AdminCreateDto getAdminCreateDto() {
        AdminCreateDto adminCreateDto = new AdminCreateDto();
        adminCreateDto.setFirstName("Viktor");
        adminCreateDto.setLastName("Ryneiski");
        adminCreateDto.setEmail("Gizzzer+" + Math.random() * 1000 + "@tut.by");
        adminCreateDto.setPassword("121287");
        return adminCreateDto;
    }
}
