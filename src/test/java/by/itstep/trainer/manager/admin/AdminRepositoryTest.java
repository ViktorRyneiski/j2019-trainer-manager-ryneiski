package by.itstep.trainer.manager.admin;

import by.itstep.trainer.manager.entity.Admin;
import by.itstep.trainer.manager.entity.enums.Role;
import by.itstep.trainer.manager.repository.AdminRepository;
import by.itstep.trainer.manager.repository.impl.AdminRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class AdminRepositoryTest {

    private final AdminRepository repository = new AdminRepositoryImpl();

    @BeforeEach
    void setUp() {
        repository.deleteAll();
    }

    @Test
    void getById_happyPath() {
        //given
        Admin admin = getAdmin();

        //when
        Admin saved = repository.create(admin);
        Admin toEquals = repository.findById(saved.getId());

        //then
        Assertions.assertEquals(admin.getClass(), toEquals.getClass());
    }

    @Test
    void testfindAll_happyPath() {
        //given
        List<Admin> admins = getListAdmins();

        for (Admin admin : admins) {
            repository.create(admin);
        }

        //when

        List<Admin> found = repository.findAll();

        //then
        Assertions.assertEquals(admins.size(), found.size());
    }

    @Test
    void testCreate_happyPath() {
        //given
        Admin admin = getAdmin();

        //when
        Admin saved = repository.create(admin);

        //then
        Assertions.assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        Admin admin = getAdmin();

        //when
        Admin saved = repository.create(admin);

        Admin toUpdate = repository.findById(saved.getId());
        toUpdate.setEmail("new");

        Admin updated = repository.update(toUpdate);

        //then
        Assertions.assertEquals(toUpdate.getEmail(), updated.getEmail());
    }

    @Test
    void testDelete_happyPath() {

        //given
        Admin admin = getAdmin();

        //when
        Admin saved = repository.create(admin);
        repository.delete(saved.getId());

        Admin toEquals = repository.findById(saved.getId());

        //then
        Assertions.assertEquals(null, toEquals);
    }

    @Test
    void testDeleteAll_happyPath() {
        //given
        List<Admin> admins = getListAdmins();

        //when
        for (Admin toSave : admins) {
            repository.create(toSave);
        }
        repository.deleteAll();

        List<Admin> adminsDeleted = repository.findAll();

        //then
        Assertions.assertEquals(0, adminsDeleted.size());
    }

    private Admin getAdmin() {
        Admin admin = Admin.builder()
                .firstName("Viktor")
                .lastName("Ryneiski")
                .email("Gizzzer@tut.by" + Math.random() * 1000)
                .password("121287")
                .role(Role.SUPERUSER)
                .build();
        return admin;
    }

    private List<Admin> getListAdmins() {
        List<Admin> admins = new ArrayList<>();
        admins.add(getAdmin());
        admins.add(getAdmin());
        admins.add(getAdmin());
        admins.add(getAdmin());
        admins.add(getAdmin());
        return admins;
    }
}
